<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Blog extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(  )
    {

        // DB::table('blog')->insert(
        //     array(
        //         'titulo'  => 'Postagem teste',
        //         'slug'    => 'postagem-teste',
        //         'post'    => '<p>Postagem de teste</p>',
        //         'user_id' => 1
        //     )
        // );

        // factory(App\User::class, 50)->create()->each(function($u) {
        //     $u->posts()->save(factory(App\Blog::class)->make());
        // });

        factory( App\Blog::class, 20 )->create(  )->each( function( $u ) {
            // $u->user()->save(factory(App\User::class)->make());
        } );

    }

}
