<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(  )
    {

        DB::table('categorias')->insert(
            array(
                'name'        => 'admin',
                'description' => 'Administador de conteudo'
            )
        );

        DB::table('categorias')->insert(
            array(
                'name'        => 'colunista',
                'description' => 'Colunista do site'
            )
        );

        DB::table('categorias')->insert(
            array(
                'name'        => 'usuario',
                'description' => 'usuário do site'
            )
        );

        /**
         *
         * Inserção de usuário.
         *
         */
        DB::table('users')->insert(
            array(
                'name'         => 'Fabio Silveira',
                'email'        => 'www.cyber.p@gmail.com',
                'password'     => Hash::make('123456'),
                'categoria_id' => 1
            )
        );

        DB::table('users')->insert(
            array(
                'name'         => 'Marilia Gabriela Ketzer',
                'email'        => 'mg_ketzer@hotmail.com',
                'password'     => Hash::make('123456'),
                'categoria_id' => 1
            )
        );

        /**
         *
         * Inserção de usuário randômico.
         *
         */
        factory( App\User::class, 5 )->create(  )->each( function( $u ) {
            // $u->categoria(  )->save( factory( App\Categoria::class )->make(  ) );
        } );

        /**
         *
         * Inserção de postagem.
         *
         */
        DB::table('blog')->insert(
            array(
                'titulo'  => 'Postagem teste',
                'slug'    => 'postagem-teste',
                'post'    => '<p>Postagem de teste</p>',
                'user_id' => 1
            )
        );

        /**
         *
         * Inserção de postagem randômica.
         *
         */
        factory( App\Blog::class, 20 )->create(  )->each( function( $u ) {
            // $u->user()->save(factory(App\User::class)->make());
        } );

        // $this->call( Blog::class );
        // factory(App\Blog::class, 20)->create()->each(function($u) {
        //     // $u->user()->save(factory(App\User::class)->make());
        // });

    }

}
