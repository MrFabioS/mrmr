<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define( App\Blog::class, function ( Faker\Generator $faker )
{

    return [
        'titulo'  => $faker->sentence,
        'slug'    => $faker->slug,
        'post'    => $faker->text,
        'user_id' => rand( 1, 2 ),
    ];

} );
