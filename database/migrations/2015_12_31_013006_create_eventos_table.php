<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
        /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'eventos', function ( Blueprint $table ) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('slug');
            $table->string('chamada');
            $table->binary('descricao');
            $table->timestamps();
        });

        DB::table('eventos')->insert(
            array(
                'titulo'    => 'Evento teste',
                'slug'      => 'evento-teste',
                'chamada'   => 'Evento de teste para testar os eventos',
                'descricao' => '<h2>Evento de teste</h2><p>Teste de eventos.</p><p>Este é um testo para testar a descrição de eventos.</p>'
            )
        );
        DB::table('eventos')->insert(
            array(
                'titulo'    => 'Evento teste 2',
                'slug'      => 'evento-teste-2',
                'chamada'   => 'Evento de teste para testar os eventos',
                'descricao' => '<h2>Evento de teste</h2><p>Teste de eventos.</p><p>Este é um testo para testar a descrição de eventos.</p>'
            )
        );
        DB::table('eventos')->insert(
            array(
                'titulo'    => 'Evento teste 3',
                'slug'      => 'evento-teste-3',
                'chamada'   => 'Evento de teste para testar os eventos',
                'descricao' => '<h2>Evento de teste</h2><p>Teste de eventos.</p><p>Este é um testo para testar a descrição de eventos.</p>'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {

        Schema::drop('eventos');

    }

}
