<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute precisa ser aceito.',
    'active_url'           => ':attribute não é uma URL valida.',
    'after'                => ':attribute precisa ser uma data posterior a :date.',
    'alpha'                => ':attribute deve conter apenas letras.',
    'alpha_dash'           => ':attribute pode conter apenas letras, números, e traços.',
    'alpha_num'            => ':attribute pode conter apenas letras e números.',
    'array'                => ':attribute precisa ser um array.',
    'before'               => ':attribute precisa ser uma data antes de :date.',
    'between'              => [
        'numeric' => ':attribute precisa ser um valor entre :min e :max.',
        'file'    => ':attribute precisa ter um tamanho entre :min e :max kilobytes.',
        'string'  => ':attribute precisa ter entre :min e :max caracteres.',
        'array'   => ':attribute precisa ter entre :min e :max itens.',
    ],
    'boolean'              => ':attribute precisa ser verdadeiro ou falso.',
    'confirmed'            => ':attribute confirmação não confere.',
    'date'                 => ':attribute não é uma data válida.',
    'date_format'          => ':attribute não combina com o formato :format.',
    'different'            => ':attribute e :other precisam ser diferentes.',
    'digits'               => ':attribute precisa ter :digits digitos.',
    'digits_between'       => ':attribute precisa ter entre :min e :max digitos.',
    'email'                => ':attribute precisa ser um endereço de e-mail valido.',
    'exists'               => ':attribute selecionado não é válido.',
    'filled'               => ':attribute é obrigatório.',
    'image'                => ':attribute precisa ser uma imagem.',
    'in'                   => ':attribute selecionado é invalido.',
    'integer'              => ':attribute precisa ser um número inteiro.',
    'ip'                   => ':attribute precisa ser um endereço de IP valido.',
    'json'                 => ':attribute precisa ser uma string JSON valida.',
    'max'                  => [
        'numeric' => ':attribute não pode ser maior que :max.',
        'file'    => ':attribute não pode ser maior que :max kilobytes.',
        'string'  => ':attribute não pode ser maior que :max caracteres.',
        'array'   => ':attribute não pode ser maior que :max itens.',
    ],
    'mimes'                => ':attribute precisa ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => ':attribute precisa ter no mínimo :min.',
        'file'    => ':attribute precisa ter no mínimo :min kilobytes.',
        'string'  => ':attribute precisa ter no mínimo :min caracteres.',
        'array'   => ':attribute precisa ter pelo menos :min items.',
    ],
    'not_in'               => ':attribute selecionado não é valido.',
    'numeric'              => ':attribute precisa ser um numero.',
    'regex'                => ':attribute formato invalido.',
    'required'             => ':attribute é um campo obrigatório.',
    'required_if'          => ':attribute é um campo obrigatório quando :other é :value.',
    'required_unless'      => ':attribute é um campo obrigatório a menos que :other esteja em :values.',
    'required_with'        => ':attribute é um campo obrigatório quando :values existir.',
    'required_with_all'    => ':attribute é um campo obrigatório quando :values existir.',
    'required_without'     => ':attribute é um campo obrigatório quando :values não existir.',
    'required_without_all' => ':attribute é um campo obrigatório quando nrnhum :values exitir.',
    'same'                 => ':attribute e :other precisam ser iguais.',
    'size'                 => [
        'numeric' => ':attribute precisa ter :size.',
        'file'    => ':attribute precisa ter :size kilobytes.',
        'string'  => ':attribute precisa ter :size characteres.',
        'array'   => ':attribute precisa ter :size itens.',
    ],
    'string'               => ':attribute precisa ser uma string.',
    'timezone'             => ':attribute precisa ser uma zona valida.',
    'unique'               => ':attribute já foi registrado.',
    'url'                  => ':attribute formato inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'mensagem customizada',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
