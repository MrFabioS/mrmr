<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha deve ter pelo menos seis caracteres e ser igual na confirmação.',
    'reset' => 'Sua senha será ressetada!',
    'sent' => 'Enviamos um e-mail para sua conta com um link para ressetar sua senha!',
    'token' => 'O token de resset de senha é inválido.',
    'user' => "Não encontramos um usuário cadastrado com este e-mail.",

];
