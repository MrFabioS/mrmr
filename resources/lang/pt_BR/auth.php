<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'A conta não está cadastrada.',
    'throttle' => 'Muitas tentativar de login. Por favor, tente novamente em :seconds segundos.',

];
