@extends('layouts.default')

@section('content')
    <!--=========== BEGIN GALLERY SECTION ================-->
    <section id="errorpage">
      <div class="container">
        <div class="error_page_content">
             <h1>404</h1>
             <h2>Ops :(</h2>
             <h3>Esta página não existe.</h3>
             <p class="wow fadeInLeftBig animated" style="visibility: visible; animation-name: fadeInLeftBig;">por favor, siga para nossa <a href="{{ url( 'home' ) }}">Home page</a></p>
           </div>
      </div>
    </section>
    <!--=========== END GALLERY SECTION ================-->
@stop