<?php

    $menus   = array(  );
    $menus[] = array( 'title'=>'Home', 'slug'=>'home', 'active'=>( url(  )->current(  )==url( 'home' ) ? 'active' : 'null' ) );
    $menus[] = array( 'title'=>'Colunas', 'slug'=>'colunas', 'active'=>( url(  )->current(  )==url( 'colunas' ) ? 'active' : 'null' ) );
    $menus[] = array( 'title'=>'Eventos', 'slug'=>'eventos', 'active'=>( url(  )->current(  )==url( 'eventos' ) ? 'active' : 'null' ) );
    $menus[] = array( 'title'=>'Galeria', 'slug'=>'galeria', 'active'=>( url(  )->current(  )==url( 'galeria' ) ? 'active' : 'null' ) );
    $menus[] = array( 'title'=>'Blog',    'slug'=>'blog',    'active'=>( url(  )->current(  )==url( 'blog' )    ? 'active' : 'null' ) );
    $menus[] = array( 'title'=>'contato', 'slug'=>'contato', 'active'=>( url(  )->current(  )==url( 'contato' ) ? 'active' : 'null' ) );

?>
<div class="container">
    <div class="navbar-header">
        <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <!-- LOGO -->
        <!-- TEXT BASED LOGO -->
        <a class="navbar-brand" href="/">Mulheres Reais, <span>Mães Reais</span></a>
        <!-- IMG BASED LOGO  -->
        <!-- <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a>  -->

    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
            <!-- Authentication Links -->
            @if (Auth::guest())
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Usuário<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{ url('/login') }}">Login</a>
                    </li>
                    <li>
                        <a href="{{ url('/register') }}">Cadastro</a>
                    </li>
                </ul>
            </li>
            @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{ url( '/meus-dados' ) }}"></i>Meus dados</a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                    </li>
                </ul>
            </li>
            @endif
            @foreach( $menus as $menu )
            <li class="{{ $menu['active'] }}">
                <a href="{{ url( $menu['slug'] ) }}">{{ $menu['title'] }}</a>
            </li>
            @endforeach

<!--
            <li class="active">
                <a href="{{ url( 'home' ) }}">Home</a>
            </li>
            <li>
                <a href="{{ url( 'colunas' ) }}">Colunas</a>
            </li>
            <li>
                <a href="{{ url( 'scholarship.html' ) }}">Scholarship</a>
            </li>
            <li>
                <a href="{{ url( 'eventos' ) }}">Eventos</a>
            </li>
            <li>
                <a href="{{ url( 'galeria' ) }}">Galeria</a>
            </li>
            <li>
                <a href="{{ url( 'blog' ) }}">Blog</a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Page<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{ url( 'erro' ) }}">404 Page</a>
                    </li>
                    <li>
                        <a href="{{ url( '#' ) }}">Link Two</a>
                    </li>
                    <li>
                        <a href="{{ url( '#' ) }}">Link Three</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ url( 'contato' ) }}">Contato</a>
            </li>
 -->
        </ul>
    </div><!--/.nav-collapse -->
</div>
