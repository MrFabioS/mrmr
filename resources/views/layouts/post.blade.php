@section( 'title', $post['titulo'] )
<!--=========== BEGIN COURSE BANNER SECTION ================-->
<section id="imgBanner">
    <h2>Course Details</h2>
</section>
<!--=========== END COURSE BANNER SECTION ================-->


<!--=========== BEGIN COURSE BANNER SECTION ================-->
<section id="courseArchive">
    <div class="container">
        <div class="row">
            <!-- start course content -->
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="courseArchive_content">
                    <div class="singlecourse_ferimg_area">
                        <div class="singlecourse_ferimg">
                            <img src="{{ asset( 'img/course-single.jpg' ) }}" alt="img">
                        </div>
                        <div class="singlecourse_bottom">
                            <h2>{{ $post['titulo'] }}</h2>
                            <span class="singlecourse_author">
                                <img alt="img" src="{{ asset( 'img/author.jpg' ) }}">
                                {{ $post->user->name }}
                            </span>
                        </div>
                    </div>
                    <div class="single_course_content">
                        <h2>{{ $post['titulo'] }}</h2>
                        {!! $post['post'] !!}
                    </div>
                    <!-- start related course -->
                    <div class="related_course">
                        <h2>Mais Postagens</h2>
                        <div class="row">
                            @foreach( $posts as $postagem )
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="single_course wow fadeInUp" >
                                    <div class="singCourse_imgarea">
                                        <img src="{{ asset( 'img/course-1.jpg' ) }}">
                                        <div class="mask">
                                            <a class="course_more" href="{{ url( 'blog/' . $postagem['slug'] . '/' . $postagem['id'] ) }}">Ver Postagem</a>
                                        </div>
                                    </div>
                                    <div class="singCourse_content">
                                        <h3 class="singCourse_title"><a href="{{ url( 'blog/' . $postagem['slug'] . '/' . $postagem['id'] ) }}">{{ $postagem['titulo'] }}</a></h3>
                                        {!! $postagem['post'] !!}
                                    </div>
                                    <div class="singCourse_author">
                                        <img alt="img" src="{{ asset( 'img/author.jpg' ) }}">
                                        <p>Richard Remus, Teacher</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- End related course -->
                </div>
            </div>
            <!-- End course content -->

            <!-- start course archive sidebar -->
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="courseArchive_sidebar">
                    <!-- start single sidebar -->
                    <div class="single_sidebar">
                        <h2>Events <span class="fa fa-angle-double-right"></span></h2>
                        <ul class="news_tab">
                            <li>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#" class="news_img">
                                            <img alt="img" src="{{ asset( 'img/news.jpg' ) }}" class="media-object">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="#">Dummy text of the printing and typesetting industry</a>
                                        <span class="feed_date">27.02.15</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#" class="news_img">
                                            <img alt="img" src="{{ asset( 'img/news.jpg' ) }}" class="media-object">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="#">Dummy text of the printing and typesetting industry</a>
                                        <span class="feed_date">28.02.15</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#" class="news_img">
                                            <img alt="img" src="{{ asset( 'img/news.jpg' ) }}" class="media-object">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="#">Dummy text of the printing and typesetting industry</a>
                                        <span class="feed_date">28.02.15</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- End single sidebar -->
                    <!-- start single sidebar -->
                    <div class="single_sidebar">
                        <h2>Quick Links <span class="fa fa-angle-double-right"></span></h2>
                        <ul>
                            <li><a href="#">Link 1</a></li>
                            <li><a href="#">Link 2</a></li>
                            <li><a href="#">Link 3</a></li>
                            <li><a href="#">Link 4</a></li>
                            <li><a href="#">Link 5</a></li>
                        </ul>
                    </div>
                    <!-- End single sidebar -->
                    <!-- start single sidebar -->
                    <div class="single_sidebar">
                        <h2>Sponsor Add <span class="fa fa-angle-double-right"></span></h2>
                        <a class="side_add" href="#"><img src="{{ asset( 'img/side-add.jpg' ) }}" alt="img"></a>
                    </div>
                    <!-- End single sidebar -->
                </div>
            </div>
            <!-- start course archive sidebar -->
        </div>
    </div>
</section>
<!--=========== END COURSE BANNER SECTION ================-->
