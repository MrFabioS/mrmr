@extends('layouts.default')

@section('content')
<!--=========== BEGIN COURSE BANNER SECTION ================-->
<section id="imgBanner">
  <h2>Login</h2>
</section>
<!--=========== END COURSE BANNER SECTION ================-->
<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">

                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="contact_form wow fadeInLeft">
                            <input type="email" class="form-control wp-form-control wpcf7-text" name="email" value="{{ old('email') }}" placeholder="Endereço de e-mail" >

                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="contact_form wow fadeInLeft">
                            <input type="password" class="form-control wp-form-control wpcf7-text" name="password" placeholder="Sua senha">

                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-sign-in"></i>Login
                            </button>

                            <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="title_area">
                    <h2 class="title_two">Cadastro</h2>
                    <span></span>
                    <p>Caso ainda não tenha cadastro em nosso site, efetue seu registro em nosso <a href="{{ url( 'register' ) }}">Cadastro</a>.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
