@extends('layouts.default')

@section('title', 'Home')

@section('content')
    @include( 'layouts.index' )
@endsection
