@extends('layouts.default')

@section('title', 'eventos')

@section('content')

<!-- start blog archive  -->
<div class="row">
    <!-- start single blog archive -->
    <div class="col-lg-12 col-12 col-sm-12">
        <div class="single_blog_archive wow fadeInUp">
            <div class="blogimg_container">
                <a href="#" class="blog_img">
                <img alt="img" src="{{  asset( 'img/blog.jpg' ) }}">
                </a>
            </div>
            <h2 class="blog_title"><a href="events-single.html"> Curabitur ac dictum nisl eu hendrerit ante</a></h2>
            <div class="blog_commentbox">
                <p><i class="fa fa-clock-o"></i>Time: 7pm,15 March 2015</p>
                <p><i class="fa fa-map-marker"></i>Location: London,UK</p>
            </div>
            <p class="blog_summary">Duis erat purus, tincidunt vel ullamcorper ut, consequat tempus nibh. Proin condimentum risus ligula, dignissim mollis tortor hendrerit vel. Aliquam...</p>
            <a class="blog_readmore" href="events-single.html">Read More</a>
        </div>
    </div>
    <!-- End single blog archive -->
</div>
<!-- end blog archive  -->

@stop

<!-- @section('content')

    @include('layouts.eventos')

@stop
 -->