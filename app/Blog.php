<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $table = 'blog';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'post',
        'user_id',

    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'slug',
    ];

    public function user(  )
    {

        return $this->belongsTo( 'App\User' );

    }

    public function data( $data )
    {

        return date_format( $data, 'd-m-Y' );

    }

}
