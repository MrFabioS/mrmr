<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['domain' => 'mrmr.fsit.com.br'], function () {
    Route::get('/', function () {
        return redirect('home');
    });
});



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// Route::group(['middleware' => ['web']], function () {
//     //
// });

Route::group(['domain' => 'admin.mrmr.fsit.com.br'], function () {
    Route::resource('/', 'Admin');
    Route::resource('adicionar', 'Admin@create');
});


Route::group(['prefix' => 'admin'], function ()
{
    Route::get('/', function () {
        return 'oi';
    });
});

Route::group(['middleware' => 'web'], function ()
{


    Route::auth();

    // Route::get('home', 'HomeController@index');

    // Route::get('colunas', 'HomeController@index');



    Route::get('home', function () {
        return View::make('home');
    });

    Route::get('meus-dados', function () {
        return View::make('meus-dados');
    });

    Route::get('colunas', function () {
        return View::make('colunas');
    });

    // Route::get('eventos', 'eventos@index');
    Route::get('eventos', function () {
        return View::make( 'eventos' );
    });

    Route::get( 'galeria', function (  )
    {

        return View::make( 'galeria' );

    } );

    Route::get( 'blog', function (  )
    {

        $posts = App\Blog::paginate( 3 );
        return View::make( 'posts', [ 'posts'=>$posts, ] );

    } );

    Route::get( 'blog/{postagem}/{id}', function ( $postagem, $id )
    {

        $posts = App\Blog::where( 'id', '!=', $id )->paginate( 2 );

        $post = App\Blog::find( $id );

        return View::make( 'post', [ 'post'=>$post, 'posts'=>$posts, ] );
    } )
    ->where( array( 'postagem'=>'[a-z0-9\-]+', 'id'=>'[0-9]+' ) );

    Route::get('contato', function () {
        return View::make('contact');
    });

});
// }
