<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Blog;

class Blog extends Controller
{
    /**
     * Show the profile for the given user.
     */
    public function showPosts( )
    {
        $posts = App\Blog::all();

        // return View::make( 'posts' )->with( array( 'nome' => '$nome' ) );

        // return 'oi';
        return View::make('posts', array('posts' => $posts));
    }
    //
}
