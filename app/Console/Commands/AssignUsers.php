<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class AssignUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:assign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign user account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( User $user )
    {
        parent::__construct();
        $this->user = $user;
        // $this->find(1)->categoria;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $usuario = array(  );
        $usuario['nome']     = $this->ask( 'nome' );
        $usuario['email']    = $this->ask( 'e-mail' );
        $usuario['password'] = $this->ask( 'password' );
        DB::table('users')->insert( $usuario );
        $this->info( $usuario );
        // $user = $this->argument( 'user' );
        // $this->info($user);

        }
}
