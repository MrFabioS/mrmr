<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eventos extends Model
{

    protected $table = 'eventos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'chamada',
        'descrição',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'slug',
    ];

}
